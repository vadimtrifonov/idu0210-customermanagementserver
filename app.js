// Module dependencies

var express = require('express');
var consolidate = require('consolidate');
var http = require('http');
var path = require('path');
var customers = require('./routes/customers');
var addresses = require('./routes/addresses');
var contacts = require('./routes/contacts');

var app = express();

// Config

app.set('port', process.env.PORT || 8401);
app.set('address', 'localhost');
app.set('core', 'http://localhost:8400');

app.engine('html', consolidate.handlebars);
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views'));

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.errorHandler());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// Index

app.get('/', function(req, res) {
  res.render('index', {title:'Customer management server', message:'Listening...'});
});

// Customers

app.get('/customers', customers.list);
app.post('/customers', customers.create);
app.post('/customers/:id', customers.update);
app.del('/customers/:id', customers.del);

// Addresses

app.get('/customers/:customerId/addresses', addresses.list);
app.post('/customers/:customerId/addresses', addresses.create);
app.post('/customers/:customerId/addresses/:id', addresses.update);
app.del('/customers/:customerId/addresses/:id', addresses.del);

// Contacts

app.get('/customers/:customerId/contacts', contacts.list);
app.post('/customers/:customerId/contacts', contacts.create);
app.post('/customers/:customerId/contacts/:id', contacts.update);
app.del('/customers/:customerId/contacts/:id', contacts.del);
app.get('/customers/contact-types', contacts.types);

http.createServer(app).listen(app.get('port'), app.get('address'), function() {
  console.log('Customer management server listening on ' + app.get('address') + ':' + app.get('port'));
});
