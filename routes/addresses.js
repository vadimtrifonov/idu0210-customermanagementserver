var request = require('request');

exports.list = function(req, res) {
    request(req.app.get('core') + '/customers/' + req.params.customerId + '/addresses', {headers:{accept:'application/json'}}, function (error, coreRes, body) {
        res.json(JSON.parse(body));
    });
};

exports.create = function(req, res) {
    request.post(req.app.get('core') + '/customers/' + req.params.customerId + '/addresses', {json: req.body}, function (error, coreRes, body) {
        res.json(201, body);
    });
};

exports.update = function(req, res) {
    request.post(req.app.get('core') + '/customers/' + req.params.customerId + '/addresses/' + req.params.id, {json: req.body}, function (error, coreRes, body) {
        res.json(body);
    });
};

exports.del = function(req, res) {
    request.del(req.app.get('core') + '/customers/' + req.params.customerId + '/addresses/' + req.params.id, function (error, coreRes, body) {
        res.json({});
    });
};