var request = require('request');

exports.list = function(req, res) {
    console.log('REQUEST HEADERS: ' + JSON.stringify(req.headers));
    console.log('REQUEST BODY: ' + JSON.stringify(req.body));
    request(req.app.get('core') + '/customers', {headers:{accept:'application/json'}}, function (error, coreRes, body) {
        console.log('CORE RESPONSE STATUS: ' + coreRes.statusCode);
        console.log('CORE RESPONSE HEADERS: ' + JSON.stringify(coreRes.headers));
        console.log('CORE RESPONSE BODY: ' + JSON.stringify(body));
        res.json(JSON.parse(body));
    });
};

exports.create = function(req, res) {
    request.post(req.app.get('core') + '/customers', {json: req.body}, function (error, coreRes, body) {
        res.json(201, body);
    });
};

exports.update = function(req, res) {
    request.post(req.app.get('core') + '/customers/' + req.params.id, {json: req.body}, function (error, coreRes, body) {
        res.json(body);
    });
};

exports.del = function(req, res) {
    request.del(req.app.get('core') + '/customers/' + req.params.id, function (error, coreRes, body) {
        res.json({});
    });
};